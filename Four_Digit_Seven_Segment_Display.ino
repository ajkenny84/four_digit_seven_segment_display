/*
Name: Four_Digit_Seven_Segment_Display.ino
Description: Common Cathode version. Displays any supplied character.
Created By: Adam Kenny
Updated on 2013-07-11 */

/*
DISPLAY PIN LOCATIONS
12  11  10  9  8  7
  _   _   _   _
 |_| |_| |_| |_|
 |_|.|_|.|_|.|_|.

1  2   3  4   5  6
*/

/*
PIN CONNECTIONS
ARDUINO   RESISTOR   DISPLAY
1         220        6
2         220        8
3         220        9
4         220        12
5         220        3
6         220        5
7         220        10
8         220        1
9         220        2
10        220        4
11        220        7
12        220        11
*/

// Set I/O pins
int digitOnes = 1;
int digitTens = 2;
int digitHundreds = 3;
int digitThousands = 4;
int decimalPoint = 5;
int g = 6;
int f = 7;
int e = 8;
int d = 9;
int c = 10;
int b = 11;
int a = 12;

int msDelay = 5; // delay to wait before switching to next display

byte seven_seg_chars[10][7] = {{1,1,1,1,1,1,0},  // = 0
                                {0,1,1,0,0,0,0}, // = 1
                                {1,1,0,1,1,0,1}, // = 2
                                {1,1,1,1,0,0,1}, // = 3
                                {0,1,1,0,0,1,1}, // = 4
                                {1,0,1,1,0,1,1}, // = 5
                                {1,0,1,1,1,1,1}, // = 6
                                {1,1,1,0,0,0,0}, // = 7
                                {1,1,1,1,1,1,1}, // = 8
                                {1,1,1,0,0,1,1}  // = 9
};

void setup(){
  for (int i=1; i<=12; i++){
    pinMode(i,OUTPUT);
  }
}

void loop(){
  SetOutput("ones");
  DisplayChar(4);
  
  delay(msDelay);
  
  SetOutput("tens");
  DisplayChar(3);
  
  delay(msDelay);
  
  SetOutput("hundreds");
  DisplayChar(2);
  // digitalWrite(decimalPoint, HIGH);
  
  delay(msDelay);

  SetOutput("thousands");
  DisplayChar(1);
  
  delay(msDelay);
}

// setting which output digit to use
void SetOutput(String place){
  if (place == "ones"){
    digitalWrite(digitOnes,LOW);
    digitalWrite(digitTens,HIGH);
    digitalWrite(digitHundreds,HIGH);
    digitalWrite(digitThousands,HIGH);
  } else if (place == "tens"){
    digitalWrite(digitOnes,HIGH);
    digitalWrite(digitTens,LOW);
    digitalWrite(digitHundreds,HIGH);
    digitalWrite(digitThousands,HIGH);
  } else if (place == "hundreds"){
    digitalWrite(digitOnes,HIGH);
    digitalWrite(digitTens,HIGH);
    digitalWrite(digitHundreds,LOW);
    digitalWrite(digitThousands,HIGH);
  } else if (place == "thousands"){
    digitalWrite(digitOnes,HIGH);
    digitalWrite(digitTens,HIGH);
    digitalWrite(digitHundreds,HIGH);
    digitalWrite(digitThousands,LOW);
  } else return;
}

// Displays character
void DisplayChar(byte character){
  byte pin = 12;
  for (byte segment=0; segment<7; segment++){
    digitalWrite(pin, seven_seg_chars[character][segment]);
    pin--;
  }
}
